#nodejs-file-watcher

Simple watcher for restart nodejs server after changes

For correct work you need add file `watcher.config.json` to root of your project.

###Config example
```json
{
  "watchers": [
    {
      "path": "auth",
      "command": "cd auth && npm run build",
      "exclude": [
        "auth/.git",
        "auth/dist"
      ]
    },
    {
      "path": "src"
    }
  ],
  "run": "ts-node -r dotenv/config src/index.ts"
}
```

#####Watchers
Array files and directories for watching.

| Field         | Description   |
| ------------- |:-------------:|
| path          | Path to files for watching |
| command       | Command which will be running after changes in path (execute before `run`) |
| exclude       | Array of directories and files which be excluded for watching |

####Run
Main command for running

