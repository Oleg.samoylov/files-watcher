#!/usr/bin/env node
const chokidar = require('chokidar');
const { spawn } = require('child_process');
const { success, info } = require('nodejs-lite-logger');
const { readFileSync } = require('fs');
const anymatch = require('anymatch');
const kill = require('tree-kill');
const json = readFileSync(`watcher.config.json`);
const config = JSON.parse(json.toString());
const { watchers, run } = config;
const spawnOptions = { stdio: 'inherit', shell: true };

let runner = spawn(run, [], spawnOptions);
watchers.forEach(({ command, exclude, path }) => {
  let child;
  chokidar.watch(path, { ignored: anymatch(exclude) }).on('change', (updatedPath) => {
    if (command) {
      child && (kill(child.pid), child = null);
      child = spawn(command, [], spawnOptions);
      child.on('close', killAndRerun.bind(this, updatedPath));
    } else {
      return killAndRerun(updatedPath);
    }
  });
});

function killAndRerun(updatedPath) {
  kill(runner.pid);
  runner = null;
  success('File changed - ' + updatedPath);
  runner = spawn(run, [], spawnOptions);
}